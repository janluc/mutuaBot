#!/bin/sh
SERVICE=mutuabot.py;

pid=`pidof python3`

#Vamos matar o processo.
#OBS: Tome cuidado porque a proxima linha mata todos os processos que utilizem python3 que estejam rodando.
kill $pid
echo "$SERVICE is not running!"
#comandos a serem executados caso o programa nao esteja sendo exec
cd /root/mutuaBot
nohup python3 ./mutuabot.py &
echo "$SERVICE running back again."
