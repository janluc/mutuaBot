import sqlite3


class DBHelper:
    def __init__(self, dbname="compras.sqlite"):
        self.dbname = dbname
        self.conn = sqlite3.connect(dbname)

    def setup(self):
        print("creating table")
        stmt = "CREATE TABLE IF NOT EXISTS cesta (chat_id integer, nomes text, grupo text, semana text, produto text, quantidade integer, preco real)"
        stmt2 = "CREATE TABLE IF NOT EXISTS inscricao (chat_id integer, nomes text, grupo text, data real)"
        stmt3 = "CREATE TABLE IF NOT EXISTS estados (chat_id integer, estado text, instante real, entrada text)"
        stmt4 = "CREATE TABLE IF NOT EXISTS semanas (grupo text, semana integer, entrega text, data real)"
        self.conn.execute(stmt)
        self.conn.execute(stmt2)
        self.conn.execute(stmt3)
        self.conn.execute(stmt4)
        self.conn.commit()

    def troca_estado(self, chat_id, estado, instante, entrada):
        stmt1 = "DELETE FROM estados WHERE chat_id = (?)"
        arg1 = (chat_id,)
        stmt2 = "INSERT INTO estados (chat_id, estado, instante, entrada) VALUES (?,?,?,?)"
        arg2 = (chat_id, estado, instante, entrada)
        self.conn.execute(stmt1, arg1)
        self.conn.execute(stmt2, arg2)
        self.conn.commit()

    def troca_semana(self, grupo, semana, entrega, data):
        stmt1 = "DELETE FROM semanas WHERE grupo = (?)"
        arg1 = (grupo,)
        stmt2 = "INSERT INTO semanas (grupo, semana, entrega, data) VALUES (?,?,?,?)"
        arg2 = (grupo, semana, entrega, data)
        self.conn.execute(stmt1, arg1)
        self.conn.execute(stmt2, arg2)
        self.conn.commit()
        
    def checa_semana(self, grupo):
        stmt = "SELECT semana, entrega FROM semanas WHERE grupo=(?)"
        args= (grupo,)
        return [x for x in self.conn.execute(stmt, args)]
    
    def checa_grupos_semanas(self):
        stmt = "SELECT grupo, semana, entrega FROM semanas"
        return [x for x in self.conn.execute(stmt)]

    def checa_todos_estados(self):
        stmt = "SELECT chat_id, estado, instante, entrada FROM estados"
        return [x for x in self.conn.execute(stmt)]    

    def checa_todas_cestas(self, grupo, semana):
        stmt = "SELECT chat_id, nomes, produto, quantidade, preco FROM cesta WHERE grupo = (?) AND semana = (?)"
        args = (grupo, semana)
        return [x for x in self.conn.execute(stmt, args)]    

    def checa_grupo(self, chat_id):
        stmt = "SELECT grupo FROM inscricao WHERE chat_id=(?)"
        args= (chat_id,)
        return [x for x in self.conn.execute(stmt, args)]    

    def checa_estado(self, chat_id):
        stmt = "SELECT estado, instante, entrada FROM estados WHERE chat_id=(?)"
        args= (chat_id,)
        return [x for x in self.conn.execute(stmt, args)]    

    def inscreve(self, chat_id, nome, grupo, data):
        stmt1 = "DELETE FROM inscricao WHERE chat_id = (?)"
        args1 = (chat_id,)
        stmt2 = "INSERT INTO inscricao (chat_id, nomes, grupo, data) VALUES (?, ?, ?, ?)"
        args2 = (chat_id, nome, grupo, data)
        self.conn.execute(stmt1, args1)
        self.conn.execute(stmt2, args2)
        self.conn.commit()
        
    def checa_inscritos(self):
        stmt = "SELECT * FROM inscricao"
        return [x for x in self.conn.execute(stmt)]

    def checa_nome(self, chat_id):
        stmt = "SELECT nomes, grupo FROM inscricao WHERE chat_id=(?)"
        args = (chat_id,)
        return [x for x in self.conn.execute(stmt, args)]

    def add_item(self, chat_id, nome, grupo, semana, produto, quantidade, preco):
        stmt = "INSERT INTO cesta (chat_id, nomes, grupo, semana, produto, quantidade, preco) VALUES (?, ?, ?, ?, ?, ?, ?)"
        args = (chat_id, nome, grupo, semana, produto, quantidade, preco)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def delete_item(self, chat_id, semana, produto):
        stmt = "DELETE FROM cesta WHERE chat_id = (?) AND semana = (?) AND produto = (?)"
        args = (chat_id, semana, produto)
        self.conn.execute(stmt, args)
        self.conn.commit()

    def get_produtos(self, chat_id, semana):
        stmt = "SELECT produto, quantidade, preco FROM cesta WHERE chat_id=(?) AND semana = (?)"
        args = (chat_id, semana)
        #resultados = self.conn.execute(stmt, args)
        return [x for x in self.conn.execute(stmt, args)]
    
    def get_quantidade_produto(self, chat_id, semana, produto):
        stmt = "SELECT quantidade FROM cesta WHERE chat_id=(?) AND semana = (?) AND produto = (?)"
        args = (chat_id, semana, produto)
        #resultados = self.conn.execute(stmt, args)
        return [x for x in self.conn.execute(stmt, args)]
#
#    def get_quantidades(self, nome, semana):
#        stmt = "SELECT quantidade FROM cesta WHERE nome=(?) AND semana = (?)"
#        args = (nome, semana)
#        return [x[0] for x in self.conn.execute(stmt, args)]
#
#    def get_preco(self, nome, semana):
#        stmt = "SELECT preco FROM cesta WHERE nome=(?) AND semana = (?)"
#        args = (nome, semana)
#        return [x[0] for x in self.conn.execute(stmt, args)]
