import requests
import time
import os
import urllib #biblioteca necessaria para a codificacao do texto em forma de url
from secretoken import tokensecret


TOKEN = tokensecret()
URL = "https://api.telegram.org/bot{}/".format(TOKEN)

global tentativas
tentativas = 0  # variavel que conta o numero de erros de conexao que obtivemos em 'get_url'

def get_url(url):
    '''
    Envia a url para o Telegram.
    '''
    try:
        response = requests.get(url).json()
        global tentativas
        tentativas =0
    except:
    #except ConnectionError:

        if tentativas>24:
            raise Exception('Nao foi possivel obter os updates depois de dois minutos de Errors')
        else:
            time.sleep(5)
            global tentativas
            tentativas = tentativas + 1
            print("{} Conexões consecutivas mal sucedidas".format(tentativas))
            response = {"ok":True,"result":[]}  #mente para o bot que nada aconteceu
    return response



def get_updates(offset=None):
    '''
    Recebe as novas mensagens.
    '''
    url = URL + "getUpdates?timeout=120"
    if offset:
        url += "&offset={}".format(offset)
    js = get_url(url)
    if len(js["result"]) > 0:
        update_ids = []
        for update in js["result"]:
            update_ids.append(int(update["update_id"]))
        next_update_id = (max(update_ids) + 1)  # Informa qual o proximo update para avisar o servidor que já lidamos com os updates anteriores.
    else:
        next_update_id = None
        
    return js, next_update_id


def get_last_chat_id_and_text(updates):
    '''
    Obtem o ultimo texto e "ID" recebido para lidar com isso.
    '''
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)

def send_message(text, chat_id, reply_markup=None):
    '''
    Envia mensagem com o texto para certo chat.
    reply_markup pode conter o json necessario para enviar um teclado personalizado.
    '''
    text = urllib.parse.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    if reply_markup:
        url += "&reply_markup={}".format(reply_markup)
    get_url(url)
    
def edit_reply_markup(chat_id, reply_markup):
    '''
    TODO: checar se isso faz a coisa certa. A ideia é ser capaz de editar o teclado inline.
    '''
    url = URL + "editMessageReplyMarkup?chat_id={}".format(chat_id)
    if reply_markup:
        url += "&reply_markup={}".format(reply_markup)
    get_url(url)


def send_pic(chat_id, file):
    url=URL+ "sendPhoto"
    files = {'photo': open('{}/{}'.format(os.getcwd(),file), 'rb')}
    data = {'chat_id' : chat_id}
    requests.post(url, files=files, data=data)
    
def send_file(chat_id, file):
    url=URL+ "sendDocument"
    files = {'document': open('{}'.format(file), 'rb')}
    data = {'chat_id' : chat_id}
    requests.post(url, files=files, data=data)