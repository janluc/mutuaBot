# MutuaBot

Bot da Rede Mútua para organização das compras coletivas.
Participantes das compras coletivas podem conversar com o bot e fazer encomendas.


### Bot para Telegram ###

Para executar o bot você deve possuir um token obtido com o @botfather. Você deve, então, inserir o token no local indicado dentro do arquivo chamado "secretoken.py":

>def tokensecret():

>    secret = "<token obtido com o BotFather>"

>    return secret

Depois disso, basta executar o bot via:

> python3 ./mutuaBot.py

## Instruções gerais ##

### Como criar uma lista da semana: ###

A forma de criar a lista pode ser aprimorada com o tempo. Por enquanto, temos que criar um arquivo .txt contendo comentários iniciados com "#" que vão ser enviados junto com a lista e seguindo de linhas com a seguinte estrutura:

> < produto\>, < preço com ponto (e não vírgula)\>, < unidade (cada 250g, molho, etc.)\>

Exemplo:

> \# Entrega dia 27/05

> \# Nessa semana a entrega será feita no ponto X

> Rúcula, 2.00, molho

> Alface, 1.50, molho

> Açaí, 5.00, cada 250g

> Limão, 3.00, 1kg

O nome do arquivo criado deverá ser:

> < nome do grupo\>\_semana< número da semana\>.txt

exemplo: cebb_semana3.txt

### Como adicionar novos grupos de compra (dev): ###

1. O nome do grupo deve ser incluído na função "disse_nome" (de interpreta_pergunta.py) nas opções para o teclado:

>     opcoes = ("CEBB", <NOVO GRUPO>)
>     keyboard = build_keyboard(opcoes, remove=True)

2. Depois disso, devemos inserir o grupo como uma opção de cadastro dentro do if da função "disse_grupo".


### Softwares necessários para utilização (dev): ###

python3 

Aparentemente todas as bibliotecas utilizadas vêm como padrão na instalação do Python3


### Como automatizar a reinicialização do BOT ###

OBS: O maior problema do script desenvolvido é que ele impede que outros programas que utilizem python3 estejam rodando concomitantemente.

Basta transferir o arquivo da pasta "extras" para a pasta do sistema "/etc/cron.hourly/".

************************

### Links úteis: ###

Informações de como alterar o número de itens de acordo com a quantidade na cesta via:
https://core.telegram.org/bots/api#editmessagereplymarkup

(Ainda não sei fazer o que é ensinado no link acima. Jan, 26/04/18.)

Os _requests_ tem que ser enviados via "OBJETO JSON", como é o caso do inline keyboard:
https://core.telegram.org/bots/api#inlinekeyboardmarkup

### Licença ###

Licença de Software Livre GPLV3. Criado por Jan Luc Tavares.
