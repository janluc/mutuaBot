                                                                                                                                                                                                                                                                                                                                                                                 #
# Programa principal do bot para compras coletivas.
# O programa principal organiza e dá o "timing" dos programas auxiliares: botengine, dbhelper e interpreta_entrada.
# Esses programas são responsáveis por, respectivamente, a interação com o servidor, a 'memória' do bot e as respostas que devem ser dadas de acordo com a entrada.
#
# TODO: 1. Fazer obtenção da tabela pronta.
# 2. Limpar codigo de "interpreta_entrada" pois tem um monte de funcoes que nao sao usadas.
# Dialogos basicos concluidos.
# 
# Jan Luc Tavares, março de 2018.


# trecho para tornar a saida utf-8 caso o computado esteja complicando a vida:
import os
import sys

#if sys.stdout.encoding != 'utf-8':
#    print("O encoding padrao do sistema nao parece ser utf-8. Vamos trocar.")
#    os.putenv("PYTHONIOENCODING",'utf-8')
#    os.execv(sys.executable,['python3']+sys.argv)
#print("Encoding: utf-8")

# Trecho para funcionar no Spyder
if len(sys.argv[0]) < 1:
    home = os.path.expanduser("~")
    os.chdir(str(home + "/doGit/mutuaBot/"))
    print("Parece que você não está executando o código a partir de uma shell convencional.")

import time
from botengine import get_updates, send_message, send_file
from interpreta_entrada import interpretador


def main():
    next_update_id = None

    while True:
        #Obtem os updates:
        updates, next_update_id = get_updates(next_update_id)
        
        #lida com cada update:
        for update in updates["result"]:
            #print (update)
            text, chat, keyboard, arquivo = interpretador(update)
            
            if arquivo:
                send_file(chat, arquivo)

            send_message(text, chat, keyboard)
        
        time.sleep(0.5)

if __name__ == '__main__':
    main()
