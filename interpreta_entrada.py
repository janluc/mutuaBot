############################## Sobre #################################
# Programa para realizar o processamento das mensagens.
# Este programa comunica-se com o banco de dados e não comunica-se com os servidores do Telegram.
#

# trecho para funcionar no spyder:
import os
import sys

if len(sys.argv[0]) < 1:
    home = os.path.expanduser("~")
    os.chdir(str(home + "/doGit/mutuaBot/"))
    print("Parece que você não está executando o código a partir de uma shell convencional.")



import json
import time
from dbhelper import DBHelper

import csv

db = DBHelper()
db.setup()


def build_keyboard(items, remove=None):
    '''
    Constroi JSON que informa o Telegram qual deve ser o teclado personalizado.
    '''
    keyboard = [[item] for item in items]
    reply_markup = {"keyboard":keyboard, "one_time_keyboard": True}
    if remove==True:
        reply_markup["remove_keyboard"]=True
    
    #de teclado: "{"one_time_keyboard": true, "remove_keyboard": true, "keyboard": [["CEBB"]]}"
    #exemplo mais completo de teclado: "{"keyboard":[[{"text":"teeeste2"}]]}"
    return json.dumps(reply_markup)

def inline_keyboard(items, calls):
    '''
    Constroi JSON que informa o teclado em linha.
    '''
    keyboard = []
    for i in range(len(items)):
        keyboard.append([{"text":items[i], "callback_data":calls[i]}])
    reply_markup = {"inline_keyboard":keyboard}
    return json.dumps(reply_markup)

############# Respostas as entradas do usuario ########################
#######################################################################
def boas_vindas(chat_id, entrada=None):
    try:
        nome = db.checa_nome(chat_id)[0][0]
        text = "Olá de novo, {}!\nSe você não é {} selecione /alterar_cadastro".format(nome, nome)
        db.troca_estado(chat_id, "selecionando0", time.time(), 'sei la')
        opcoes = ("ver os produtos da semana",)
        keyboard = build_keyboard(opcoes)
    except IndexError:
        text = "Oi!\nVocê está falando com o Bot de compras coletivas da Mútua.\nReceba minhas sinceras boas-vindas!"
        text = text + " Qual o seu nome?"
        db.troca_estado(chat_id, "cadastramento0", time.time(), 'sei la')
        keyboard = None
    
    return text, keyboard, None

def recadastro(chat_id, entrada=None):
    text="ATENÇÃO:\nTome cuidado ao alterar o seu cadastro. Evite trocar de grupo, pois registramos a sua cesta de acordo com o seu grupo. Se você escolher um grupo diferente do seu, podem acontecer algumas confusões.\n"
    text = text + "Nas próximas mensagens vamos fazer o seu recadastramento. Você pode retornar pra cá sempre que necessário.\n"
    text = text + "Infome o nome que você quer cadastrado:"
    keyboard = None
    db.troca_estado(chat_id, "cadastramento0", time.time(), entrada)
    return text, keyboard, None
    
def disse_nome(chat_id, entrada=None):
    '''
    Depois de informar o nome, pedimos o grupo do qual a pessoa faz parte. Devemos incluir nessa lista, e depois tomar a atitude certa de acordo com a opcao incluida em "disse_grupo".
    '''
    text = "Olá, {}. Você faz parte de qual grupo de compras coletivas?".format(entrada.split()[0])
    db.troca_estado(chat_id, "cadastramento1", time.time(), entrada)
    opcoes = ("CEBB", "Pólo_Infantil", "Restaurantes")
    keyboard = build_keyboard(opcoes, remove=True)
    #print (keyboard)
    return text, keyboard, None

def disse_grupo(chat_id, entrada=None):
    '''
    Resposta sobre o grupo. Aqui devem ser adicionados novos grupos, bem como em "disse_nome".
    '''
    nome = db.checa_estado(chat_id)[0][2]
    if entrada.split()[0] == "CEBB" or entrada.split()[0] == "cebb" or entrada.split()[0] == "/CEBB":
        db.inscreve(chat_id, nome, "cebb", time.time())
        db.troca_estado(chat_id, "selecionando0", time.time(), "feito")
        text = "Ótimo. Agora você tem uma cesta de produtos e pode acrescentar nela os produtos dessa semana.\n"
        opcoes = ("ver os produtos da semana","conferir minha cesta")
        keyboard = build_keyboard(opcoes)
    elif entrada.split()[0] == "polo_infantil" or entrada.split()[0] == "Polo_infantil" or entrada.split()[0] == "Pólo_infantil" or entrada.split()[0] == "polo-infantil" or entrada.split()[0] == "Pólo_Infantil":
        db.inscreve(chat_id, nome, "polo_infantil", time.time())
        db.troca_estado(chat_id, "selecionando0", time.time(), "feito")
        text = "Ótimo. Agora você tem uma cesta de produtos e pode acrescentar nela os produtos dessa semana.\n"
        opcoes = ("ver os produtos da semana","conferir minha cesta")
        keyboard = build_keyboard(opcoes)
    elif entrada.split()[0] == "resturantes" or entrada.split()[0] == "Restaurantes":
        db.inscreve(chat_id, nome, "restaurantes", time.time())
        db.troca_estado(chat_id, "selecionando0", time.time(), "feito")
        text = "Ótimo. Agora você tem uma cesta de produtos e pode acrescentar nela os produtos dessa semana.\n"
        opcoes = ("ver os produtos da semana","conferir minha cesta")
        keyboard = build_keyboard(opcoes)
    else:
        text = "Voce disse um grupo que não existe ainda. :(\nQual o nome do grupo que você faz parte?"
        opcoes = ("CEBB",  "Polo_Infantil", "Restaurantes")
        keyboard = build_keyboard(opcoes, remove=True)   
    
    return text, keyboard, None

#def cesta_vs_produtos(chat_id, entrada=None):
#    if entrada == "ver os produtos da semana":
#        text, keyboard = checa_produtos(chat_id, entrada)
#        #db.troca_estado(chat_id, "produtos0", time.time(), "feito")
#    elif entrada == "conferir minha cesta":
#        text, keyboard = cesta(chat_id, entrada)
#        #db.troca_estado(chat_id, "cesta0", time.time(), "feito")
#    else:
#        text = "Você deu uma resposta inválida. Selecione uma das opções que surgiram"
#        keyboard = None
#    return text, keyboard, None

def checa_produtos(chat_id, entrada=None):
    db.troca_estado(chat_id, "escolhendo produtos", time.time(), entrada)
    try:
        text = "Lista de produtos da semana \n"
        grupo = db.checa_grupo(chat_id)[0][0]
        semana = db.checa_semana(grupo)[0][0]
        endereco = os.getcwd() + "/ofertas/{}_semana{}.txt".format(grupo, semana) 
        produtos = []
        precos = []
        unidade = []
        lista = []
        arq = open(endereco)
        for line in arq:
            li=line.split(',')
            if (not li[0].startswith("#") and len(

li) > 2) : #ignora linhas com comentarios
                produtos.append(li[0])
                precos.append(li[1])
                unidade.append(li[2])
                text = text + "{} - {} - R$ {:.2f} \n".format(produtos[-1],unidade[-1][:-1],float(precos[-1]))
            else:
                text = text + line[1:-1] + "\n"
        arq.close()
        lista = produtos
        text = text + "\n Toque uma vez nos itens abaixo para adicionar uma unidade do produto (se você tocar várias vezes, vai adicionar várias unidades, mesmo que eu demore um pouco pra responder): \n\nCaso queira remover algum item da sua lista, isto é possível clicando em \"conferir minha cesta.\n"
        keyboard = inline_keyboard(lista, produtos)
        
    except:
        text = "Algo deu errado enquanto procurávamos a lista de produtos. Talvez ainda não tenhamos lista para essa semana. Tente de novo em algumas horas."
        keyboard = None
        
    return text, keyboard, None
    
def adicionar(chat_id, produto=None):
    try:
        grupo = db.checa_grupo(chat_id)[0][0]
        semana = db.checa_semana(grupo)[0][0]
        endereco = os.getcwd() + "/ofertas/{}_semana{}.txt".format(grupo, semana) 
        produtos = []
        precos = []
        unidade = []


        arq = open(endereco)
        for line in arq:
            li=line.split(',')
            if (not li[0].startswith("#")) : #ignora linhas com comentarios
                produtos.append(li[0])
                precos.append(li[1])
                unidade.append(li[2])
        arq.close()
        
        try:
            quantidade = int (db.get_quantidade_produto(chat_id, semana, produto)[0][0]) + 1 #checa a quantidade que já constava.
        except:
            quantidade = 1
        try:
            indice = produtos.index(produto)
            nome = db.checa_nome(chat_id)[0][0]
            db.delete_item(chat_id, semana, produto)
            preco = float(precos[indice])*quantidade
            #print(chat_id, nome, grupo, semana, produto, quantidade, preco)
            db.add_item(chat_id, nome, grupo, semana, produto, quantidade, preco)
            text = "Adicionado.\nVocê já colocou {} unidades de {}\nSubtotal do item: R$ {:.2f}".format(quantidade, produto, preco)
        except ValueError:
            text = "Você disse um produto que não consta na lista dessa semana."
    except:
        text = "aconteceu algo de estranho"

    opcoes = ("conferir minha cesta",)
    keyboard = build_keyboard(opcoes)
    return text, keyboard, None

def cesta(chat_id, entrada=None):
    try:
        grupo = db.checa_grupo(chat_id)[0][0]
        semana = db.checa_semana(grupo)[0][0]
        lista = db.get_produtos(chat_id, semana)
        text = "Sua lista de produtos;\n QUANTIDADE - PRODUTO  | PRECO\n"
        precos = []
        for linha in lista:
            precos.append(linha[2])
            text = "{}{} - {}:  R$ {:.2f} \n".format(text, str(linha[1]), str(linha[0]), linha[2]) #quantidade, item, preco
        
        text = text + "\nTotal da cesta: R$ {:.2f}".format(sum(precos))
        opcoes = ("Não gostei, quero remover itens","Colocar mais coisas", "Excelente! Finalizar compra.")
        calls = ("remover","adicionar mais", "finalizar", )
        keyboard = inline_keyboard(opcoes, calls)
    except IndexError:
        text = "Parece que você ainda não fez o cadastramento."
        opcoes = ["/alterar_cadastro"]
        keyboard = build_keyboard(opcoes)
    return text, keyboard, None

def listar_remover(chat_id, entrada=None):
    grupo = db.checa_grupo(chat_id)[0][0]
    semana = db.checa_semana(grupo)[0][0]
    lista = db.get_produtos(chat_id, semana)
    text = "Selecione o item que quer remover da lista:"
    itens = []
    calls = []
    for linha in lista:
        calls.append("rem "+ str(linha[0]))
        itens.append(str(linha[0]))
    keyboard = inline_keyboard(itens, calls)
    return text, keyboard, None


def remover(chat_id, produto=None):
    try:
        grupo = db.checa_grupo(chat_id)[0][0]
        semana = db.checa_semana(grupo)[0][0]
        db.delete_item(chat_id, semana, produto)
        text = "Removido {}. Basta adicionar novamente caso queira um número menor.".format(produto)
    except:
        text = "aconteceu algo de estranho"

    opcoes = ("adicionar mais produtos","conferir minha cesta")
    keyboard = build_keyboard(opcoes)
    return text, keyboard, None


def finaliza_compra(chat_id, entrada=None):
    grupo = db.checa_grupo(chat_id)[0][0]
    semana = db.checa_semana(grupo)[0][0]
    entrega = db.checa_semana(grupo)[0][1]
    text = "Você concluiu sua compra! Receba {} no local combinado.\nConfira sua lista de produtos;\n QUANTIDADE - PRODUTO  | PRECO\n".format(entrega)
    lista = db.get_produtos(chat_id, semana)
    precos = []
    for linha in lista:
        precos.append(linha[2])
        text = "{}{} - {}:  R$ {:.2f} \n".format(text, str(linha[1]), str(linha[0]), linha[2]) #quantidade, item, preco
    
    text = text + "\nTotal da cesta: R$ {:.2f}".format(sum(precos))
    opcoes = ["Já busquei meus itens."]
    calls = ["buscou"]
    keyboard = inline_keyboard(opcoes, calls)
    arquivo = None
    entrada="{}semana{}".format(grupo,semana)
    db.troca_estado(chat_id, "compra concluida", time.time(), entrada)
    
    return text, keyboard, arquivo

def buscou_produtos(chat_id, entrada =None):
    grupo = db.checa_grupo(chat_id)[0][0]
    semana = db.checa_semana(grupo)[0][0]    
    entrada= "{}semana{}".format(grupo, semana)
    todos_estados = db.checa_todos_estados()
    ultimo_a_buscar=True
    for estado in todos_estados:
        if entrada and "compra concluida" in estado:
            ultimo_a_buscar=False
    if ultimo_a_buscar:
        text = "Você foi a última pessoa a buscar seus itens, considere harmonizar o local.\nAté a próxima :D"
    else:
        text = "Ótimo, me procure no próximo período de compras para ver os produtos da semana :)"
    
    db.troca_estado(chat_id, "Selecionando0", time.time(), entrada)
    opcoes = ["ver os produtos da semana"]
    keyboard = build_keyboard(opcoes)
    return text, keyboard, None

def ajuda(chat_id,entrada=None):
    text="/start - Boas-vindas\nVocê quer alterar seu nome ou o grupo do qual faz parte? Basta /alterar_cadastro. \n"
    text = text + "Quer conferir os produtos da semana? /produtos\n"
    text = text + "Quer conferir o que tem na sua cesta? /cesta\n"
    text = text + "Quer pedir outras ajudas? Fale com a Laura.\n"
    text = text + "Quer dar um feedback sobre o bot? Fale com o @janluc"
    keyboard = None
    return text, keyboard, None



#######################################################################################
############ FUNCIONALIDADES EXTRA - PARA A PESSOA QUE ORGANIZA A COMPRA ##############
#######################################################################################
def obtem_tabela(chat_id, entrada=None):
    try:
        grupo = entrada.split()[1]
        semana = db.checa_semana(grupo)[0][0]
    except:
        grupo = "polo_infantil"
        semana = db.checa_semana(grupo)[0][0]
        
    try:
        endereco_arq_csv = "{}/relatorios/{}_semana{}.csv".format(os.getcwd(),grupo, semana)
        compras_semana = db.checa_todas_cestas(grupo, semana)
        #compras_semana é uma lista, cheia de linhas na forma "chat_id, nomes, produto, quantidade, preco"
        
        
        # quero resultado tipo volume_produtos = {"Produto": ["Volume", "Unidade", "Subtotal do produto"]}
        volume_produtos = {}
        
        endereco = os.getcwd() + "/ofertas/{}_semana{}.txt".format(grupo, semana) 
        produtos = []
        unidade = []
        arq = open(endereco)
        for line in arq:
            li=line.split(',')
            if (not li[0].startswith("#") and len(li) > 2) : #ignora linhas com comentarios
                produtos.append(li[0])
                unidade.append(li[2].rstrip("\n"))
        
        pessoas = []
        for linha in compras_semana:
            if not ([linha[0], linha[1]] in pessoas):
                pessoas.append([linha[0], linha[1]])
                
            if linha[2] in volume_produtos.keys():
                volume_produtos[linha[2]][0] += linha[3]
                volume_produtos[linha[2]][2] += linha[4]
            else:
                try:
                    volume_produtos[linha[2]] =[linha[3], unidade[produtos.index(linha[2])], linha[4] ] 
                except:
                    volume_produtos[linha[2]] =[linha[3], "nao_encontrado", linha[4]]
        
        produtos_encomendados = list(volume_produtos.keys())
        
        #opção 1 -> tabela vertical
        nomes_pessoas = []
        for pessoa in pessoas:
            nomes_pessoas.append(pessoa[1])
        tabela_por_produto = [["v ITEM > PESSOA:"] + nomes_pessoas ]
        for produto in produtos_encomendados:
            tabela_por_produto.append([produto])
            for pessoa in pessoas:
                try:
                    tabela_por_produto[-1].append(db.get_quantidade_produto(pessoa[0], semana, produto)[0][0])
                except:
                    tabela_por_produto[-1].append(" ")
#        
#        # Acrescentando a última linha com o total a pagar:
        tabela_por_produto.append(["Total a pagar (R$):",])

        for pessoa in pessoas:
            lista = db.get_produtos(pessoa[0], semana)
            precos = []
            for linha in lista:
                precos.append(linha[2])
            tabela_por_produto[-1].append(sum(precos))        
        
        ## opção 2 -> tabela horizontal
        #tabela_por_produto = [["v PESSOA > ITEM:"] + produtos_encomendados ]
        #for pessoa in pessoas:
        #    tabela_por_produto.append([pessoa[1]])
        #    for produto in produtos_encomendados:
        #        try:
        #            tabela_por_produto[-1].append(db.get_quantidade_produto(pessoa[0], semana, produto)[0][0])
        #        except:
        #            tabela_por_produto[-1].append(" ")
        
        #compras_semana.insert(0, ("identificador", "Nome", "Produto", "Quantidade", "Subtotal (R$)"))
        #endereco_arq_csv = "{}/relatorios/teste{}_semana{}.csv".format(os.getcwd(),grupo, semana)
        
        
        with open(endereco_arq_csv, "w") as output:
            writer = csv.writer(output)
            writer.writerow(volume_produtos.keys())
            writer.writerows(zip(*volume_produtos.values()))
        # Gambiarra master
        transposta = zip(*csv.reader(open(endereco_arq_csv, "r")))
        transposta = list(transposta)
        transposta.insert(0, ["Produto", "Volume", "Unidade", "Subtotal (R$)"])
        transposta.append("")
        for line in tabela_por_produto:
            transposta.append(line)
        csv.writer(open(endereco_arq_csv, "w")).writerows(transposta)
        
        text = "Ta na mao o CSV contendo a tabela da semana {}".format(semana)
    
    except:
        text = "Parece que o grupo ainda não está rolando."
        endereco_arq_csv = None
    keyboard = None
    
    return text, keyboard, endereco_arq_csv

def obtem_status(chat_id, entrada=None):
    estados = db.checa_todos_estados() #estados é uma lista, cheia de linhas na forma "chat_id, estado, instante, entrada"
    estados.insert(0, ("identificador", "estado", "instante que entrou nesse estado", "ultima informacao"))
    inscricoes= db.checa_inscritos() #inscricoes é uma lista, cheia de linhas na forma "chat_id, nomes, grupo, data"
    inscricoes.insert(0, ("identificador", "nome", "grupo", "data da inscricao"))
    tabela = inscricoes + estados
    endereco_arq_csv = "{}/relatorios/inscricoes_estados.csv".format(os.getcwd())
    with open(endereco_arq_csv, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(tabela)
    
    text = "Nessa tabela tem as inscrições e os estados de cada pessoa. Assim dá pra saber quem finalizou a compra."
    keyboard = None
    
    return text, keyboard, endereco_arq_csv

def trocar_semana(chat_id, entrada=None):
    if len(entrada.split()) >3 :
        entrada = entrada[13:]
        grupo = entrada.split()[0]
        if grupo == "Cebb" or grupo == "CEBB":
            grupo = "cebb"
        elif grupo == "polo_infantil" or grupo == "Polo_infantil" or grupo == "Pólo_infantil" or grupo == "polo-infantil" or grupo == "Pólo_Infantil":
            grupo="polo_infantil"
        elif grupo == "Restaurantes" or grupo == "restaurantes":
            grupo="restaurantes"
        corte = len(grupo) + 1
        entrega = entrada[corte:]
        text = "Trocando semana: \n\n"
        try:
            semana_anterior =  db.checa_semana(grupo)[0][0]
        except IndexError:
            semana_anterior = 0
            text = text + "Você criou a semana ZERO para o grupo \"{}\". \n".format(grupo)
        semana = semana_anterior + 1
        db.troca_semana(grupo, semana, entrega, time.time())
    
        text=text + "Você alterou a semana do grupo \"{}\" de {} para {}.\nVocê disse que a entrega é: \"{}\"".format(grupo, semana_anterior, semana, entrega)
        itens = ["Quero inserir manualmente a semana ou alterar a entrega"]
        calls = ["semana_manual"]
        keyboard = inline_keyboard(itens, calls)
    else:
        text = "deu algo errado"
        keyboard = None
    
    return text, keyboard, None
    
def semana_manual1(chat_id, entrada=None):
    text = "Para informar a semana manualmente, você deve escrever bem bonitinho da seguinte forma a próxima mensagem:\n"
    text = text + "<grupo> <semana> <entrega>\n\n"
    text = text + "Onde <grupo> é o nome do grupo em minúsculas, <semana> é o número da semana para o qual você quer alterar e <entrega> é uma mensagem que você diz sobre a data da entrega (exemplo 'dia 03/09 no galpão X')"
    keyboard = None
    db.troca_estado(chat_id, "altera_semana", time.time(), "informando_manualmente")
    return text, keyboard, None

def semana_manual2(chat_id, entrada=None):
    grupo = entrada.split()[0]
    if grupo == "Cebb" or grupo == "CEBB":
        grupo = "cebb"
    elif grupo == "polo_infantil" or grupo == "Polo_infantil" or grupo == "Pólo_infantil" or grupo == "polo-infantil" or grupo == "Pólo_Infantil":
        grupo="polo_infantil"
    elif grupo == "Restaurantes" or grupo == "restaurantes":
        grupo="restaurantes"

    semana = entrada.split()[1]
    corte = len(grupo) + len(semana) + 2
    entrega = entrada[corte:]
    text = "Trocando semana: \n\n"
    try:
        semana_anterior =  db.checa_semana(grupo)[0][0]
    except IndexError:
        semana_anterior = 0
        text = text + "Você criou a semana ZERO para o grupo {}. \n".format(grupo)
    db.troca_semana(grupo, semana, entrega, time.time())
    
    text=text + "Você alterou a semana do grupo {} de {} para {}.\nVocê disse que a entrega é: {}".format(grupo, semana_anterior, semana, entrega)
    db.troca_estado(chat_id, "selecionando0", time.time(), "informando_manualmente")
    keyboard = None
    return text, keyboard, None

def nova_lista(chat_id, entrada=None):
    
    text = "Diz o nome do grupo seguido da semana: (exemplo \"cebb 12\") \n\n (minusculo e sem acento)"
    
    keyboard = None
    db.troca_estado(chat_id, "nova_lista0", time.time(), "informando_nova_lista")
    return text, keyboard, None

def nova_lista1(chat_id, entrada=None):
    grupo = entrada.split()[0]
    semana = entrada.split()[1]
    text = "Grupo: {} \nSemana: {}\nCole abaixo o texto da lista dessa semana. \nEsta é a maneira mais facil de fazer isso.\n Você pode também dizer \"cancelar\".".format(grupo, semana)
    
    keyboard = build_keyboard(["cancelar"])
    infos = grupo + " " + semana
    db.troca_estado(chat_id, "nova_lista1", time.time(), infos)
    return text, keyboard, None

def nova_lista2(chat_id, entrada=None):
    if entrada == "cancelar":
        text= "Ok, você cancelou."
    else:
        grupo = db.checa_estado(chat_id)[0][2].split()[0]
        semana = db.checa_estado(chat_id)[0][2].split()[1]
        endereco = os.getcwd() + "/ofertas/{}_semana{}.txt".format(grupo, semana) 
        with open(endereco, "w") as text_file:
            text_file.write(entrada)
        text = "Feito, você salvou a lista com sucesso."
    opcoes = ["ver os produtos da semana", "inicio"]
    keyboard = build_keyboard(opcoes)
    db.troca_estado(chat_id, "selecionando0", time.time(), "informou nova lista e voltou para selecionando")
    return text, keyboard, None


def comandos_lau(chat_id, entrada=None):
    text = "Os comandos especiais disponíveis:\n\n"
    text = text + "\"status\" - Responde com uma tabela .csv com a lista de pessoas inscritas no BOT e seus estados;\n\"nova lista\" - Inicia conversa para envio de nova lista de produtos de algum grupo.\n\"relatório\" <nome do grupo> - Pega todas as cestas de compras daquele grupo em tabela .csv\n\"trocar semana\" <nome do grupo> <data da entrega> - Passa para a próxima semana de um grupo.\n\nLembre-se que os nomes dos grupos sao em minusculas."
    opcoes = ["status", "nova lista"]    
    keyboard = build_keyboard(opcoes)
    return text, keyboard, None
###############################################################################################
############ Fim das funcionalidades EXTRA - PARA A PESSOA QUE ORGANIZA A COMPRA ##############
###############################################################################################


def interpretador(update):
    '''
    Faz a conferência de estado, procura no dicionario as perguntas e devolve a resposta.
    Atualmente essa função faz as coisas de uma forma bastante "espaguete". Isso deve ser melhorado.
    '''        
#    def lista_perguntas():
#        endereco = os.getcwd() + "/perguntas_respostas.txt" #os.getcwd() retorna o diretorio corrente de trabalho.
#        arq = open(endereco)
#        for line in arq:
#            dicionario = json.loads(line)        palavras = update["message"]["text"]
#        return dicionario
#    dicionario = lista_perguntas()
    
    CB = None
    if "edited_message" in update.keys():
        chat = update["edited_message"]["chat"]["id"]
        palavras = update["edited_message"]["text"]
    elif "callback_query" in update.keys():
        CB = True
        callback = update["callback_query"]["data"]
        chat = update["callback_query"]["message"]["chat"]["id"]
        palavras = "Palavras apenas"
    elif "text" in update["message"].keys():
        chat = update["message"]["chat"]["id"]
        palavras = update["message"]["text"]
    else:
        palavras = "Ue"
        chat = update["message"]["chat"]["id"]

    #as entradas a seguir nao dependem do estado. 
    entradas = { "/start" : boas_vindas,
                "início": boas_vindas,
                "Início": boas_vindas,
                "ver os produtos da semana" : checa_produtos,
                "adicionar mais produtos": checa_produtos,
                "/produtos": checa_produtos,
                "conferir minha cesta":cesta,
                "Cesta":cesta,
                "cesta": cesta,
                "/cesta":cesta,
                "/ajuda": ajuda,
                "ajuda" : ajuda,
                "me ajude" :ajuda,
                "ajude":ajuda,
                "socorro": ajuda,
                "/alterar_cadastro": recadastro,
                "/recadastramento" : recadastro,
                "status": obtem_status,
                "nova lista": nova_lista,
                "Nova lista": nova_lista,
                "Comandos lau" : comandos_lau,
                "Comandos Lau": comandos_lau,
                "comandos Lau": comandos_lau,
                "comandos lau": comandos_lau
                }
    estados = {"cadastramento0" : disse_nome ,
               "cadastramento1" : disse_grupo,
               "altera_semana": semana_manual2,
               "nova_lista0":nova_lista1,
               "nova_lista1":nova_lista2}
    if CB:
        if callback.split()[0] == "rem":
            resposta, keyboard, arquivo = remover(chat, callback[4:])
        elif callback == "remover":
            resposta, keyboard, arquivo = listar_remover(chat)
        elif callback == "finalizar":
            resposta, keyboard, arquivo = finaliza_compra(chat)
        elif callback == "semana_manual":
            resposta, keyboard, arquivo = semana_manual1(chat)
        elif callback == "adicionar mais":
            resposta, keyboard, arquivo = checa_produtos(chat)
        elif callback == "buscou":
            resposta, keyboard, arquivo = buscou_produtos(chat)
        else:
            resposta, keyboard, arquivo = adicionar(chat, callback)

    elif palavras.split()[0] == "relatorio" or palavras.split()[0] == "relatório" or palavras.split()[0] == "Relatório":
        resposta, keyboard, arquivo = obtem_tabela(chat, palavras)
    elif palavras.split()[0] == "trocar" or palavras.split()[0] == "Trocar":
        resposta, keyboard, arquivo = trocar_semana(chat, palavras)
        
    else:
        try:
            estado = db.checa_estado(chat)[0][0]
            print("alguém passou por aqui no estado " + estado)
            #palavras = palavras.split()
            resposta, keyboard, arquivo = estados[estado](chat, palavras)
        
        except:
            try:
                resposta, keyboard, arquivo = entradas[palavras](chat, palavras)
                
            except KeyError:            
                resposta = "Não entendi. Você precisa de /ajuda?"
                keyboard = None
                arquivo =None
    return resposta, chat, keyboard, arquivo


def main():
    while True:
        print("Fale alguma coisa:")
        entrada = input()
        print(type(entrada))
        update = {'update_id': 000, 'message': {'from': {'language_code': 'pt-BR', 'is_bot': False, 'first_name': 'Exemplo', 'username': 'exemplo', 'id': 1111}, 'chat': {'first_name': 'exemplo', 'username': 'exemplo', 'type': 'private', 'id': 11111}, 'message_id': 128, 'date': 1521409162, 'text': entrada}}
        resposta, chat, keyboard, arquivo = interpretador(update)
        print("Resposta:")
        print(resposta)
    
if __name__ == '__main__':
    main()
